# Démonstration Knative

## Prérequis

### Cluster

Dans cette démonstration nous allons utiliser ```minikube``` (avec docker) pour mettre en place un cluster Kubernetes d'un seul noeud avec Knative Serving et Eventing préinstallé.

## Démonstration Knative Serving

### Service

Pour déployer notre application, nous allons créer un service Knative avec l'outil ```kn``` :

```
kn service create hello \
--image ghcr.io/knative/helloworld-go:latest \
--port 8080 \
--env TARGET=World
```

### Autoscaling

Nous allons ensuite observer l'autoscaling de notre application en envoyant une requête sur l'url de notre service. Si tout se passe bien, on passera de 0 pods à 1 ou 2.
Pour observer éxécutez dans un premier temps la commande :

```
kubectl get pod -l serving.knative.dev/service=hello -w
```

Maintenant tous les changements sur le nombre de pods seront visibles en temps réel. Ensuite dans un autre terminal, éxécutez la commande :

```
curl "$(kn service describe hello -o url)"
```

On observe bien que les pods sont lancé en fonction des requêtes qui arrivent sur le service.

### Traffic Splitting

Si nous mettons à jour notre service Knative, par exemple en changeant une variable d'environnement, on aura une nouvelle revision de notre version. On aura donc deux revisions pour les deux versions de notre applications. Knative nous donne la possibilité de choisir la quantité du traffic qui sera redirigé vers chacune de ces revisions.

D'abord, nous allons changer la variable environnement :

```
kn service update hello \
--env TARGET=Knative
```

Ici, sans précision de notre part, tout le traffic sera redirigé vers la nouvelle version: 

```
# kn revisions list
NAME          SERVICE   TRAFFIC   TAGS   GENERATION   AGE     CONDITIONS   READY   REASON
hello-00002   hello     100%             2            45s     4 OK / 4     True    
hello-00001   hello                      1            3h21m   3 OK / 4     True 
```

Donc on va définir la répartition pour avoir aussi une partie du traffic redirigé vers l'ancienne version :

```
kn service update hello \
--traffic hello-00001=50 \
--traffic @latest=50
```

Maintenant le traffic est divisé entre les deux versions :

```
# kn revisions list
NAME          SERVICE   TRAFFIC   TAGS   GENERATION   AGE     CONDITIONS   READY   REASON
hello-00002   hello     50%              2            68s     3 OK / 4     True    
hello-00001   hello     50%              1            3h21m   3 OK / 4     True   
```

Si on fait plusieurs curl sur l'URL de notre application, on observe que le traffic est bien partagé :

```
# curl "$(kn service describe hello -o url)"
Hello Knative!
# curl "$(kn service describe hello -o url)"
Hello World!
# curl "$(kn service describe hello -o url)"
Hello World!
# curl "$(kn service describe hello -o url)"
Hello Knative!
```

## Démonstration Knative Functions

Commençons par créer notre fonction :

```
kn func create -l go hello
```

Cela va créer les fichiers associés à notre fonction avec un template  :

```
# tree .
.
└── hello
    ├── func.yaml
    ├── go.mod
    ├── handle.go
    ├── handle_test.go
    └── README.md
```

Avant de déployer notre fonction, il faut paramétrer un registry qui recevra l'image build par Knative :

```
export FUNC_REGISTRY="docker.io/no0x"
```

Maintenant pour déployer notre fonction dans le cluster, éxécutez les commandes :

```
cd hello/
kn func deploy hello
```

Knative va créer un service et un URL associé, on peut tester l'assibilité à notre fonction :

```
curl "$(kn func describe hello -o url)"
```

Nous avons le retour par défaut puisque nous n'avons pas modifié le template de la fonction : 

```
GET / HTTP/1.1 hello.default.10.98.21.189.sslip.io
  X-Forwarded-Proto: http
  X-Request-Id: 8840f70c-df0a-49c0-93ae-f09fe7a35905
  User-Agent: curl/7.81.0
  Accept: */*
  Forwarded: for=10.244.0.53;proto=http
  K-Proxy-Request: activator
  X-Forwarded-For: 10.244.0.53, 10.244.0.48
```

Maintenant si on va modifier la fonction ```prettyPrint``` dans le fichier ```handle.go``` pour simplement retourner un message :

```
func prettyPrint(req *http.Request) string {
	return "Hello KnativeFunc!\n"
}
```

Et qu'on redéploie notre fonction :

```
kn func deploy hello
```

Knative va rebuild notre image et créer une deuxième version de notre service. Nous pouvons vérifié qu'il a été mis à jour avec un curl et on obtient :

```
# curl "$(kn func describe hello -o url)"
Hello KnativeFunc!
```

## Démonstration Knative Eventing

Pour pouvoir rendre disponible les évènements, nous avons déjà un broker de disponible dans notre cluster :

```
# kubectl get broker
NAME             URL                                                                               AGE   READY   REASON
example-broker   http://broker-ingress.knative-eventing.svc.cluster.local/default/example-broker   3d    True
```

Maintenant nous devons créer notre source d'évènements, pour ça nous allons utiliser une image fournie par Knative qui permet de simuler des évènements à partir d'un service. Ce service sera donc à la fois notre source et celui qui traite l'évènement :

```
kn service create cloudevents-player \
--image quay.io/ruben/cloudevents-player:latest
```

Maintenant on doit spécifier à notre service sur quel broker il doit envoyer les évènements :

```
kn source binding create ce-player-binding --subject "Service:serving.knative.dev/v1:cloudevents-player" --sink broker:example-broker
```

Nous sommes capables d'envoyer des évènements via l'interface, mais aucun service ne le reçoit, pour ça nous devons créer un trigger :

```
kn trigger create cloudevents-trigger --sink cloudevents-player  --broker example-broker
```

Maintenant en renvoyant un évènement, on peut voir que le service le reçoit également.